import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {Role} from '../../models/role';
import {RoleGuard} from '../../core/guards/role.guard';
import {CoursComponent} from './cours/cours.component';

const routes: Routes = [
  {
    path: 'coursChange',
    component: CoursComponent,
    canActivate: [RoleGuard],
    data: { roles: [Role.SU, Role.CoursDeChange]},

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursAfficheurRoutingModule { }
