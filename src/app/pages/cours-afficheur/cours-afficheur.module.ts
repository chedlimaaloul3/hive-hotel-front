import {LOCALE_ID, NgModule} from '@angular/core';
import {CommonModule, registerLocaleData} from '@angular/common';

import { CoursAfficheurRoutingModule } from './cours-afficheur-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UIModule} from '../../shared/ui/ui.module';
import {CoursComponent} from './cours/cours.component';
import localeFr from '@angular/common/locales/fr';
import { AfficheurComponent } from './afficheur/afficheur.component';
registerLocaleData(localeFr);

@NgModule({
  declarations: [CoursComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UIModule,
    CoursAfficheurRoutingModule
  ],
  providers:
  [
    {
      provide: LOCALE_ID,
      useValue: 'fr-FR'
    }

  ]
})
export class CoursAfficheurModule { }
