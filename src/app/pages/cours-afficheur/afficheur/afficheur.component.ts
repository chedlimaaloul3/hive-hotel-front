import {Component, OnInit} from '@angular/core';
import {UserServiceService} from '../../../services/authentification/userService/user-service.service';
import {CoursChangeService} from '../../../services/cours-de-change/cours-change.service';
import * as moment from 'moment-timezone';
import {Router} from '@angular/router';

@Component({
  selector: 'app-afficheur',
  templateUrl: './afficheur.component.html',
  styleUrls: ['./afficheur.component.scss']
})
export class AfficheurComponent implements OnInit {

  bureauNom: any;
  currentday: any;
  currenttime: any;
  CoursData1: any[] = [];
  CoursData2: any[] = [];

  constructor(private userService: UserServiceService,
              private coursChangeService: CoursChangeService,
              private route: Router) {
  }

  ngOnInit(): void {
    document.getElementsByTagName('body')[0].style.background = 'linear-gradient(to top, rgb(15, 32, 39), rgb(32, 58, 67), rgb(44, 83, 100))';
    this.currentday = moment().tz('Africa/Tunis').format('dddd DD MMMM YYYY');
    this.getCurrentDate();
    this.getBureauCours();

  }
  getCurrentDate() {
    setInterval(() => {
      this.currenttime = moment().tz('Africa/Tunis').format('HH:mm:ss');
    }, 1000);
  }
  getBureauCours() {
    this.CoursData1 = [];
    this.CoursData2 = [];
    this.userService.whoAmI().subscribe(
      (userId) => {
        this.userService.getUserInfo(userId.id).subscribe(
          (userInfo) => {
            const navigate = userInfo.roles.filter(x => x === 'admin insight' ).length > 0;
            if(navigate)
            {
              document.getElementsByTagName('body')[0].style.background = '#f8f8fb';

              this.route.navigate(['/home'])

            }
            this.bureauNom = userInfo.bureau.nom;
            this.coursChangeService.getBureauCours(userInfo.bureau.id).subscribe(
              (coursData) => {
                let half_length = Math.ceil(coursData.length / 2);

                this.CoursData1 = coursData.splice(0, half_length);
                this.CoursData2 = coursData.splice(0 , coursData.length );
                console.log(this.CoursData1);
                console.log(this.CoursData2);

              }
            );
          }
        );
      }
    );

  }


}
