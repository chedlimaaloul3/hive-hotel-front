import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../../../services/authentification/userService/user-service.service';
import {CoursChangeService} from '../../../services/cours-de-change/cours-change.service';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {CaisseService} from '../../../services/caisse/caisse.service';

@Component({
  selector: 'app-cours',
  templateUrl: './cours.component.html',
  styleUrls: ['./cours.component.scss']
})
export class CoursComponent implements OnInit {
  devisesData: any[] = [];
  Cours_form: FormGroup;

  constructor(private fb: FormBuilder,
              private userService: UserServiceService,
              private coursChangeService: CoursChangeService,
              private caisseService: CaisseService,
              private toastr: ToastrService
  ) {
    this.Cours_form = this.fb.group({
      cours_list: this.fb.array([]),
    });
  }

  ngOnInit(): void {

    this.getBureauCours();
  }

  getBureauCours()
  {

    this.userService.whoAmI().subscribe(
      (userId) =>
      {
        this.userService.getUserInfo(userId.id).subscribe(
          (userInfo) =>
          {
            this.getCaisseDevises(userInfo.caisse.id);
            this.coursChangeService.getBureauCours(userInfo.bureau.id).subscribe(
              (coursData) =>
              {

                coursData.forEach(item => {
                  this.CoursformData().push(this.fields(item));

                });
              }
            );
          }
        );
      }
    );

  }

  getCaisseDevises(caisseId)
  {
    this.devisesData = [];

    this.caisseService.GetCaisseDevises(caisseId).subscribe(
      (devisesData) =>
      {
        this.devisesData = devisesData;
      }
    );

  }
  CoursformData(): FormArray {
    return this.Cours_form.get('cours_list') as FormArray;
  }
  fields(data): FormGroup {
    return this.fb.group({
      id: data.id,
      nom: [data.nom],
      logo: data.logo,
      unite: data.unite,
      achat: [data.achat, [Validators.required , Validators.min(0)]],
      bureauId: data.bureauId  ,
    });
  }

  valider()
  {
    console.log(this.Cours_form.getRawValue().cours_list);

    this.coursChangeService.updateCoursChange(this.Cours_form.getRawValue().cours_list).subscribe(
      (res) =>
      {
        this.toastr.success('Cours de change  a été mise à jour avec succès', '', {closeButton: true, timeOut: 2000});
        const cf = this.Cours_form.get('cours_list')  as FormArray;
        cf.clear();

        this.getBureauCours();

      },
      (error2) =>
      {
        this.toastr.error(error2.error.error.message, 'Erreur', {closeButton: true, timeOut: 2000});

      }
    );
  }
}
