
const rolesData: any[] = [
  {
    id: '1',
    name: 'Super utilisateur',
  },
  {
    id: '2',
    name: 'Droit Achat',
  },
  {
    id: '3',
    name: 'Droit Versement caisse',
  },
  {
    id: '4',
    name: 'Droit Retrait caisse',
  },
  {
    id: '5',
    name: 'Droit Mouvement caisse',
  },
  {
    id: '6',
    name: 'Droit Ajustement caisse',
  },
  {
    id: '7',
    name: 'Droit Annulation',
  },
  {
    id: '8',
    name: 'Droit Consultation solde caisse',
  },
  {
    id: '9',
    name: 'Position',
  },
  {
    id: '10',
    name: 'Journal',
  },
  {
    id: '11',
    name: 'Journée BC',
  },
  {
    id: '12',
    name: 'Cours de change',
  },
  {
    id: '13',
    name: 'Gestion utilisateur',
  },
  {
    id: '14',
    name: 'Gestion client',
  },
  {
    id: '15',
    name: 'Droit versement banque',
  },
  {
    id: '16',
    name: 'Cession sur le comptoir',
  },

  {
    id: '17',
    name: 'Négociation cours',
  },

  {
    id: '18',
    name: 'SED',
  },
  {
    id: '19',
    name: 'Edition operation',
  },
  {
    id: '20',
    name: 'Livre des operations',
  },
  {
    id: '21',
    name: 'Afficheur',
  },
  {
    id: '22',
    name: 'Devise',
  },
  {
    id: '23',
    name: 'Caisse dépense',
  },
  {
    id: '24',
    name: 'Etat caisse',
  },
  {
    id: '25',
    name: 'Consultation profits',
  },
  {
    id: '26',
    name: 'Edition transactions',
  },
  {
    id: '27',
    name: 'Edition information',
  },
  {
    id: '28',
    name: 'Ajustement stock coupure',
  },



];
export {rolesData};
