import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {SignupService} from '../../../services/authentification/signup.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {rolesData} from '../../../core/data/rolesData';
import {BureauService} from '../../../services/bureau/bureau.service';
import {DeviseOption} from '../../../core/models/devise.model';
import {AsyncAction} from 'rxjs/internal/scheduler/AsyncAction';
import {deviseData} from '../../../core/data/deviseData';
import {DeviseService} from '../../../services/devise/devise.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-creation-compte',
  templateUrl: './creation-compte.component.html',
  styleUrls: ['./creation-compte.component.scss']
})
export class CreationCompteComponent implements OnInit {
  breadCrumbItems: Array<{}>;
  signupUserForm: FormGroup;
  roles: any[] = [];
  bureaux: any[] = [];
  selectedRoles: any[] = [];
  devises: DeviseOption[] = [];
  devisesData: any[] = [];


  constructor(private formBuilder: FormBuilder,
              private toastr: ToastrService,
              private signupService: SignupService,
              private bureauService: BureauService,
              private deviseService: DeviseService) {

  }

  ngOnInit(): void {
    this.roles = rolesData;
    this.getBureaux();
    this.devises = deviseData;



    this.breadCrumbItems = [{label: 'Création Utilisateur'}, {label: '', active: true}];
    this.signupUserForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: [''],
      code: [''],
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      username: ['', Validators.required],
      roles: [null, Validators.required],
      bureauId: [null, Validators.required],
      cin: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]],
    });

  }

  getBureaux() {
    this.bureauService.GetBureaux().subscribe(
      (bureaux) => {
        this.bureaux = bureaux;
      }
    );
  }

  get UserForm() {
    return this.signupUserForm.controls;
  }

  getValues(event) {
    this.selectedRoles = [];
    if (event) {

      event.forEach(role => {
        this.selectedRoles.push(role.name);
      });
      this.signupUserForm.get('roles').setValue(this.selectedRoles);
    }
    else {
      this.signupUserForm.get('roles').setValue(null);
    }
  }

  getIdBCT(event) {

  }

  addUserCompte() {
    this.signupUserForm.get('code').setValue('');
    this.signupUserForm.get('password').setValue('');


    this.signupService.AddUser(this.signupUserForm.getRawValue()).subscribe(
      (data) => {
        console.log(data);

        this.devisesData = [];
        this.devises.forEach(x => {
          this.devisesData.push({nom: x.name, logo: x.flag.substr(16), montant: 0.0, contreVal: 0.0 , caisseId: data.caisse.id});
        });
        this.deviseService.AddAllDevises(this.devisesData).subscribe(
          (devises) =>
          {
            this.toastr.success('Utilisateur  créé avec succès', '', {closeButton: true, timeOut: 2000});
            this.signupUserForm.reset();

          },
          error2 => {
            this.toastr.error(error2.error.error.message, '', {closeButton: true, timeOut: 2000});

          }
        );

      },
      error2 => {
        this.toastr.error(error2.error.error.message, '', {closeButton: true, timeOut: 2000});

      }
    );


  }


}
