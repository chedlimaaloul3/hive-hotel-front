import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CreationCompteComponent} from './creation-compte/creation-compte.component';
import {RoleGuard} from '../../core/guards/role.guard';
import {Role} from '../../models/role';
import {CreationBureauComponent} from './creation-bureau/creation-bureau.component';

const routes: Routes = [
  {
    path: 'creation-bureau',
    component: CreationBureauComponent,
    canActivate: [RoleGuard],
    data: { roles: [Role.AdminInsight]},

  },
  {
    path: 'creation-compte',
    component: CreationCompteComponent,
    canActivate: [RoleGuard],
    data: { roles: [Role.AdminInsight]},

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
