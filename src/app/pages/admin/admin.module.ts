import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import {UIModule} from '../../shared/ui/ui.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {CreationBureauComponent} from './creation-bureau/creation-bureau.component';
import {CreationCompteComponent} from './creation-compte/creation-compte.component';


@NgModule({
  declarations: [CreationBureauComponent, CreationCompteComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UIModule,
    NgSelectModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
