import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {BureauService} from '../../../services/bureau/bureau.service';
import {CoursChangeService} from '../../../services/cours-de-change/cours-change.service';
import {DeviseOption} from '../../../core/models/devise.model';
import {deviseData} from '../../../core/data/deviseData';

@Component({
  selector: 'app-creation-bureau',
  templateUrl: './creation-bureau.component.html',
  styleUrls: ['./creation-bureau.component.scss']
})
export class CreationBureauComponent implements OnInit {
  breadCrumbItems: Array<{}>;
  signupBCForm: FormGroup;
  img: any;
  devises: DeviseOption[] = [];
  devisesData: any[] = [];
  constructor(private formBuilder: FormBuilder,
              private toastr: ToastrService,
              private bureauService: BureauService,
              private courschangeService: CoursChangeService) {

  }

  ngOnInit(): void {
    this.devises = deviseData;

    this.breadCrumbItems = [{label: 'Création Bureau'}, {label: '', active: true}];

    this.signupBCForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      idBCT: ['', Validators.required],
      nom: ['', Validators.required],
      adresse: ['', Validators.required],
      telephone: ['', [Validators.required, Validators.pattern('[0-9]{8}')] ],
      logo: [''],
    });
  }
  get BCForm() {
    return this.signupBCForm.controls;
  }
  fileEvent(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      // me.modelvalue = reader.result;
      this.img = reader.result;
      console.log(reader.result);
    };

  }
  addBCCompte() {
    this.signupBCForm.get('logo').setValue('');

    this.bureauService.AddBC(this.signupBCForm.getRawValue()).subscribe(
      (data) => {
        this.devisesData = [];
        this.devises.forEach(x => {
          if(x.name !== 'Dinar tunisien')
            this.devisesData.push({nom: x.name, logo: x.flag.substr(16), currency: x.id, unite: x.unite, achat: 0.0 , bureauId: data.id});
        });

        this.courschangeService.AddAllCoursDeChange(this.devisesData).subscribe(
          (coursChnage) =>
          {
            this.toastr.success('Bureau  créé avec succès', '', {closeButton: true, timeOut: 2000});
            this.signupBCForm.reset();
          },
          error2 => {
            this.toastr.error(error2.error.error.message, '', {closeButton: true, timeOut: 2000});

          }
        );


      },
      error2 => {
        this.toastr.error(error2.error.error.message, '', {closeButton: true, timeOut: 2000});

      }
    );
  }
}
