import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationBureauComponent } from './creation-bureau.component';

describe('CreationBureauComponent', () => {
  let component: CreationBureauComponent;
  let fixture: ComponentFixture<CreationBureauComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreationBureauComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationBureauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
