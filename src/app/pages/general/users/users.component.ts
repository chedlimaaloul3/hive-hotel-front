import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {UserServiceService} from '../../../services/authentification/userService/user-service.service';
import {BureauService} from '../../../services/bureau/bureau.service';
import {Role} from '../../../models/role';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {rolesData} from '../../../core/data/rolesData';
import Swal from 'sweetalert2';
import {MustMatch} from '../../../Validator/must-match.validator';
import {ResetPasswordService} from '../../../services/resetpassword/reset-password.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  @ViewChild('edituser') editUser;
  @ViewChild('changePWD') changePWD;

  bureauUsersData: any[] = [];
  pageSize = 5;
  page = 1;
  breadCrumbItems: Array<{}>;
  EditUserForm: FormGroup;
  changePwdForm: FormGroup;
  item: any;
  userId: any;
  roles: any [] = [];
  isSU: boolean;
  hideRole: boolean;
  email: any;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private toastr: ToastrService,
              private userService: UserServiceService,
              private bureauService: BureauService,
              private resetePasswordService: ResetPasswordService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.breadCrumbItems = [{label: 'Liste Utilisateurs'}, {label: '', active: true}];
    this.roles = rolesData;

    this.getBureauUsers();

  }

  getBureauUsers() {
    this.userService.whoAmI().subscribe(
      (userID) => {
        this.userId = userID.id;
        this.userService.getUserInfo(userID.id).subscribe(
          (userInfo) => {
            const userRole = userInfo.roles;
            this.isSU = userRole.indexOf(Role.SU) > -1;
            this.bureauUsersData=[]
            if(this.isSU)
            {
              this.bureauService.getBureauUsers(userInfo.bureau.id).subscribe(
                (bureauUsersData) => {
                  this.bureauUsersData = bureauUsersData;
                  console.log(this.bureauUsersData);
                }
              );
            }
            else {
              this.userService.getUserInfo(userInfo.id).subscribe(
                (bureauUsersData) => {
                  this.bureauUsersData.push( bureauUsersData);
                  console.log(this.bureauUsersData);
                }
              );
            }
            }



        );
      }
    );
  }

  desactiverUser(userdata) {

    Swal.fire({
      title: 'vous êtes sur de désactiver ce compte ! ',
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      },
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Oui',
      cancelButtonText: 'Non',

    }).then((result) => {
      if (result.isConfirmed) {

        const user: any = Object.assign({}, userdata);

        user.IsDeleted = true;
        delete user.caisse;
        this.userService.editUser(user, user.id).subscribe(
          (res) => {
            this.toastr.success('Utilisateur  désactivée avec succès', '', {closeButton: true, timeOut: 2000});
            this.getBureauUsers();

          },
          error2 => {
            this.toastr.error(error2.error.message, '', {closeButton: true, timeOut: 2000});

          }
        );

      }
    });

  }

  get UserForm() {
    return this.EditUserForm.controls;
  }

  get changepwdForm() {
    return this.changePwdForm.controls;
  }

  openEditModal(data) {
    this.EditUserForm = this.formBuilder.group({
      id: [data.id],
      email: [data.email, [Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      code: [data.code],
      nom: [data.nom, Validators.required],
      prenom: [data.prenom, Validators.required],
      username: [data.username, Validators.required],
      roles: [data.roles, Validators.required],
      bureauId: [data.bureauId, Validators.required],
      cin: [data.cin, [Validators.required, Validators.minLength(8), Validators.maxLength(8)]],
      creatdate: [data.creatdate],
      firstConnexion: [data.firstConnexion],
      IsDeleted: [data.IsDeleted],
    });
    this.hideRole = this.userId === data.id;
    if (this.hideRole) {
      this.EditUserForm.controls.roles.disable();

    }

    this.modalService.open(this.editUser, {centered: true, size: 'xl', windowClass: 'modal-holder'}).result.then((result) => {
      // this.getComptes();
    }, reason => {
      // this.getComptes();
    });
  }

  getValues(event) {
    const selectedRoles = [];
    console.log(event);
    if (event) {

      event.forEach(role => {
        selectedRoles.push(role.name);
      });
      this.EditUserForm.get('roles').setValue(selectedRoles);
    }
    else {
      this.EditUserForm.get('roles').setValue(null);
    }
  }

  editUserCompte() {
    this.userService.editUser(this.EditUserForm.getRawValue(), this.EditUserForm.get('id').value).subscribe(
      (user) => {
        this.toastr.success('Utilisateur  modifiée avec succès', '', {closeButton: true, timeOut: 2000});
        this.getBureauUsers();
      },
      error2 => {
        this.toastr.error('une erreur se produite', 'Erreur', {closeButton: true, timeOut: 2000});

      }
    );
  }

  openChangePwdModal(email) {

    this.email = email;
    this.changePwdForm = this.formBuilder.group({
      actualpassword: ['', [Validators.required]],
      resetKey: [''],
      password: ['', [Validators.required]],
      confirmPassword: ['', Validators.required]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
    this.resetePasswordService.resetPasswordFirstLogin({email: this.email}).subscribe(
      (keydata: any) => {
        this.changePwdForm.get('resetKey').setValue(keydata.key);
      }
    );
    this.modalService.open(this.changePWD, {centered: true, size: 'md', windowClass: 'modal-holder'}).result.then((result) => {
        console.log('ok');
    }, (reason) => {
      console.log('ok');

    });
  }

  changerPWD() {

    const data = {email : this.email , password : this.changePwdForm.get('actualpassword').value};
    this.resetePasswordService.checkCredentials(data).subscribe(
      (user) =>
      {
        const resetpasswordData = { resetKey: this.changePwdForm.get('resetKey').value,
          password: this.changePwdForm.get('password').value,
          confirmPassword: this.changePwdForm.get('confirmPassword').value};

        this.resetePasswordService.resetPassword(resetpasswordData).subscribe(
            (res) =>
            {
              this.toastr.success('Changement de mot de passe terminée avec succès', '', { timeOut: 3000, closeButton: true});

              this.modalService.dismissAll();
              setTimeout(() => {
                localStorage.removeItem('token');
                localStorage.removeItem('expired');
                this.router.navigate(['account/login']);
            }, 3000);

            },
            error2 => {
              this.toastr.error('une erreur se produite', 'Erreur', {closeButton: true, timeOut: 2000});

            }
          );
      },
        error2 =>
      {
        this.toastr.error('Vérifier votre mot de passe actuel', 'Erreur', {closeButton: true, timeOut: 3000});

      }
    );

  }
}
