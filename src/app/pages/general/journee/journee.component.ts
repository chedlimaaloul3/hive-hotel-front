import { Component, OnInit } from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {UserServiceService} from '../../../services/authentification/userService/user-service.service';
import {JourneeService} from '../../../services/journee/journee.service';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-journee',
  templateUrl: './journee.component.html',
  styleUrls: ['./journee.component.scss']
})
export class JourneeComponent implements OnInit {
  bureauJourneData: any [] = [];
  have_opened_day: boolean = true;
  userData: any;
  pageSize = 5;
  page = 1;
  constructor( private toastr: ToastrService,
               private userService: UserServiceService,
               private journeeService: JourneeService) { }

  ngOnInit(): void {
    this.getBureauJournee();
  }

  getBureauJournee()
  {
    this.bureauJourneData = [];

    this.userService.whoAmI().subscribe(
      (userId) =>
      {
        this.userService.getUserInfo(userId.id).subscribe(
          (userData) =>
          {
            this.userData = userData;
            this.journeeService.GetBureauJournees(userData.bureau.id).subscribe(
              (bureauJourneeData) =>
              {
                this.bureauJourneData = bureauJourneeData;

                this.have_opened_day = this.bureauJourneData.filter(x => x.etat === 'ouverte' ).length > 0;
                console.log(this.have_opened_day);

              }
            );
          }
        );
      }
    );
  }

  openDay()
  {
    const journee = {code_user_ouv: this.userData.code, etat: 'ouverte', bureauId: this.userData.bureau.id};

    this.journeeService.OpenDay(journee).subscribe(
      (journeeData) =>
      {
        this.getBureauJournee();
      },
      (error2) => {
        this.toastr.error('une erreur se produite', '', {closeButton: true, timeOut: 2000});

      }
    );
    console.log(journee);
  }

  closeDay(data)
  {
    const journee: any = Object.assign({} , data);

    journee.etat = 'fermé';
    journee.code_user_ferm = this.userData.code;
    journee.date_fermuture = moment().tz('Africa/Tunis').format('MM/DD/YYYY HH:mm:ss');
    this.journeeService.closeDay(journee).subscribe(
      (res) =>
      {
        this.getBureauJournee();

      },
      (error2) =>
      {
        this.toastr.error('une erreur se produite', '', {closeButton: true, timeOut: 2000});

      }
    );

  }

}
