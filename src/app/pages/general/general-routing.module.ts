import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {Role} from '../../models/role';
import {RoleGuard} from '../../core/guards/role.guard';
import {UsersComponent} from './users/users.component';
import {JourneeComponent} from './journee/journee.component';

const routes: Routes = [

  {
    path: 'users',
    component: UsersComponent,
    canActivate: [RoleGuard],
    data: { roles: [Role.SU, Role.GestionUtilisateur]},

  },
  {
    path: 'journee',
    component: JourneeComponent,
    canActivate: [RoleGuard],
    data: { roles: [Role.SU, Role.JourneeHo]},

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GeneralRoutingModule { }
