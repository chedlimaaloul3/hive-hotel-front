import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GeneralRoutingModule } from './general-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UIModule} from '../../shared/ui/ui.module';
import {NgbAlertModule, NgbDropdownModule, NgbPaginationModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import {UsersComponent} from './users/users.component';
import {CreationCompteComponent} from '../admin/creation-compte/creation-compte.component';
import {AdminModule} from '../admin/admin.module';
import {NgSelectModule} from '@ng-select/ng-select';
import { JourneeComponent } from './journee/journee.component';


@NgModule({
  declarations: [UsersComponent, JourneeComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    UIModule,
    NgbDropdownModule,
    NgbPaginationModule,
    NgbTooltipModule,
    NgSelectModule,
    NgbAlertModule,
    GeneralRoutingModule,
  ]
})
export class GeneralModule { }
