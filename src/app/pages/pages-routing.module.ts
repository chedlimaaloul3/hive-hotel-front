import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import {RoleGuard} from '../core/guards/role.guard';
import {HomeComponent} from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home' },
  { path: 'home', component: HomeComponent ,  canActivate: [RoleGuard]},
  { path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule) },
  { path: 'suivie', loadChildren: () => import('./cours-afficheur/cours-afficheur.module').then(m => m.CoursAfficheurModule) },
  { path: 'general', loadChildren: () => import('./general/general.module').then(m => m.GeneralModule) },




];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
