import { Injectable } from '@angular/core';
import {Global} from '../../GlobalVariable/Global';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DeviseService {

  url = Global.apiURL;
  constructor(private Http: HttpClient) { }

  AddAllDevises(devises): Observable<any> {
    return this.Http.post<any>(this.url + 'devises/creatAll', devises);
  }
}
