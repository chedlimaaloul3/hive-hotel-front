import { Injectable } from '@angular/core';
import {Global} from '../../GlobalVariable/Global';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService {

  url = Global.apiURL;
  constructor(private Http: HttpClient) { }

  checkCredentials(data){
    return this.Http.post(this.url + 'users/verifCredentials', data);
  }
  forgotPassword(data){
    return this.Http.post(this.url + 'reset-password/init', data);
  }
  resetPasswordFirstLogin(data) {
    return this.Http.post(this.url + 'reset-password-first-login/init', data);
  }

  resetPassword(data) {
    return this.Http.put(this.url + 'reset-password/finish', data);
  }
}
