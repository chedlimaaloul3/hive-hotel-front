import { Injectable } from '@angular/core';
import {Global} from '../../GlobalVariable/Global';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JourneeService {
  url = Global.apiURL;
  constructor(private Http: HttpClient) { }

  GetBureauJournees(idBureau): Observable<any[]>
  {
    return this.Http.get<any[]>(this.url + '/bureaus/' + idBureau + '/journees');
  }

  OpenDay(data)
  {
    return this.Http.post(this.url + 'journees', data);
  }
  closeDay(data)
  {
    return this.Http.put(this.url + 'journees/' + data.id, data);
  }
}
