import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Global} from '../../GlobalVariable/Global';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  url = Global.apiURL;
  constructor(private Http: HttpClient) { }

  AddUser(User): Observable<any> {
    User.bureauId = User.bureauId.id
    return this.Http.post<any>(this.url + 'signupUser', User);
  }

  signupBC(compte): Observable<any> {
    return this.Http.post<any>(this.url + 'signupBC', compte);
  }
}
