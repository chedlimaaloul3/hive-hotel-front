import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Global} from '../../../GlobalVariable/Global';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  url = Global.apiURL;
  constructor(private Http: HttpClient) { }
  whoAmI(): Observable<any> {
    return this.Http.get(this.url + 'whoAmI');
  }

  getUserInfo(id): Observable<any>
  {
    return this.Http.get<any>(this.url + 'users/' + id);

  }
  editUser(user, id ) {
    return this.Http.put(this.url + 'users/' + id , user);
  }

}
