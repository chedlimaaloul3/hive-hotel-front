import { Injectable } from '@angular/core';
import {Global} from '../../../GlobalVariable/Global';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url = Global.apiURL;
  public  userID;
  constructor(private Http: HttpClient) {

  }



  login(login): Observable<any>{
    return this.Http.post(this.url + 'users/login', login);
  }

  whoAmI(): Observable<any> {
    return this.Http.get(this.url + 'whoAmI');
  }

  CheckToken(token): Observable<any> {
    return this.Http.get<any>(this.url + 'checkToken/' + token);
  }
  getUserInfo(id): Observable<any>
  {
    return this.Http.get<any>(this.url + 'users/' + id);

  }

}
