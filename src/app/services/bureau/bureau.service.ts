import { Injectable } from '@angular/core';
import {Global} from '../../GlobalVariable/Global';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BureauService {
  url = Global.apiURL;
  constructor(private Http: HttpClient) { }



  AddBC(bureau): Observable<any> {
    return this.Http.post<any>(this.url + 'bureaux', bureau);
  }
  GetBureaux(): Observable<any[]> {
    return this.Http.get<any[]>(this.url + 'bureaux' );
  }

  getBureauUsers(bureauId): Observable<any>
  {
    return this.Http.get<any[]>(this.url + 'bureaus/' + bureauId + '/users');
  }
}
