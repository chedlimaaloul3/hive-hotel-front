import { Injectable } from '@angular/core';
import {Global} from '../../GlobalVariable/Global';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CaisseService {

  url = Global.apiURL;
  constructor(private Http: HttpClient) { }


  GetCaisseDevises(caisseId): Observable<any[]>
  {
    return this.Http.get<any[]>(this.url + '/caisses/' + caisseId + '/devises');
  }
}
