import { TestBed } from '@angular/core/testing';

import { CoursChangeService } from './cours-change.service';

describe('CoursChangeService', () => {
  let service: CoursChangeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoursChangeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
