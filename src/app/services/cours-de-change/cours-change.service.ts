import { Injectable } from '@angular/core';
import {Global} from '../../GlobalVariable/Global';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CoursChangeService {

  url = Global.apiURL;
  constructor(private Http: HttpClient) { }

  AddAllCoursDeChange(coursDeChange): Observable<any> {
    return this.Http.post<any>(this.url + 'cours-de-changes/creatAll', coursDeChange);
  }
  getBureauCours(bureauId): Observable<any[]>
  {
    return this.Http.get<any[]>(this.url + '/bureaus/' + bureauId + '/cours-de-changes');
  }

  updateCoursChange(data)
  {
    return this.Http.put(this.url + 'cours-de-changes/update', data);
  }

}
