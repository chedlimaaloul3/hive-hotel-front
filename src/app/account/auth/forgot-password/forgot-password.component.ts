import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {ResetPasswordService} from '../../../services/resetpassword/reset-password.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgottForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private resetPasswordService: ResetPasswordService,
              private toastr: ToastrService) { }

  ngOnInit(): void {

    this.forgottForm = this.formBuilder.group({
        email: ['', [Validators.required , Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]]
      }
    );

  }

  get f() { return this.forgottForm.controls; }


  onSubmit() {
    this.resetPasswordService.forgotPassword(this.forgottForm.getRawValue()).subscribe(
      (data) =>
      {
        this.toastr.success('Un e-mail a été envoyé à l\'e-mail fourni avec les instructions de réinitialisation du mot de passe',
          '', { timeOut: 3000, closeButton: true});
      },
      error2 => {
        this.toastr.error(error2.error.error.message,
          '', { timeOut: 3000, closeButton: true});
      }
    );
  }
}
