import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';


import {ActivatedRoute, Router} from '@angular/router';

import {LoginService} from '../../../services/authentification/login/login.service';
import {Global} from '../../../GlobalVariable/Global';
import {ResetPasswordService} from '../../../services/resetpassword/reset-password.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

/**
 * Login component
 */
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  error = '';
  returnUrl: string;
  // set the currenr year
  year: number = new Date().getFullYear();

  // tslint:disable-next-line: max-line-length
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute,
              private router: Router,
              private authenticationService: LoginService,
              private ResetPasswordService: ResetPasswordService ,
              ) {


  }

  ngOnInit() {


    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['', [Validators.required]],
    });

    // reset login status
    // this.authenticationService.logout();
    // get return url from route parameters or default to '/'
    // tslint:disable-next-line: no-string-literal
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
    console.log(this.returnUrl);
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  /**
   * Form submit
   */
  onSubmit() {

    // stop here if form is invalid

    this.authenticationService.login(this.loginForm.getRawValue()).subscribe(
      (res) => {
        localStorage.setItem('token', res.token);
        localStorage.setItem('expired', 'false');
        this.checkFirstConnexion();


      },
      error => {
        this.error = error ? error.error.error.message : '';
        console.log(this.error);
      }
    );



  }
  checkFirstConnexion()
  {
    this.authenticationService.whoAmI().subscribe(
      (userId: any) => {
        localStorage.setItem('userID', userId.id);

        this.authenticationService.getUserInfo(userId.id).subscribe(
          (user) => {
            if (user.firstConnexion === 1) {
              this.resetPasswordRequest({email: user.email});
            }
            else
            {
              this.router.navigate([this.returnUrl]);

            }



          }
        );

      }
    );
  }

  // updateUserStatus() {
  //   this.authenticationService.whoAmI().subscribe(
  //     (userId) => {
  //       this.authenticationService.userInfo(userId.id).subscribe(
  //         (user) => {
  //           if (user.firstConnexion === 1) {
  //             this.resetPasswordRequest({email: user.email});
  //           }
  //
  //           user.status = 'online';
  //           console.log(user)
  //           this.CompteService.editCompte(user, user.id).subscribe(
  //             (data) => {
  //             }
  //           );
  //           this.router.navigate([this.returnUrl]);
  //
  //         }
  //       );
  //
  //     }
  //   );
  // }

  resetPasswordRequest(data) {
    this.ResetPasswordService.resetPasswordFirstLogin(data).subscribe(
      (key: any) =>
      {
        this.router.navigate(['/account/reset-password', key.key]);
        return;

      }
    );

  }


}
