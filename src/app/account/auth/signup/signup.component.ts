import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {AuthenticationService} from '../../../core/services/auth.service';
import {environment} from '../../../../environments/environment';
import {first} from 'rxjs/operators';
import {UserProfileService} from '../../../core/services/user.service';
import {ToastrService} from 'ngx-toastr';
import {SignupService} from '../../../services/authentification/signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;

  // set the currenr year
  year: number = new Date().getFullYear();

  // tslint:disable-next-line: max-line-length
  constructor(private formBuilder: FormBuilder,
              private toastr: ToastrService,
              private router: Router,
              private signupService: SignupService) {
  }

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['', Validators.required],
      Nom_Commercial: ['', Validators.required],
      MatF: ['', Validators.required],
    });


  }

  // convenience getter for easy access to form fields
  get f() {
    return this.signupForm.controls;
  }

  /**
   * On submit form
   */
  onSubmit() {

    // this.signupService.signupEntrep(this.signupForm.getRawValue()).subscribe(
    //   (data) => {
    //     this.toastr.success('Compte  créé avec succès', '', {closeButton: true, timeOut: 2000});
    //     // this.signupForm.reset();
    //     // setTimeout(() => {
    //     //     this.router.navigate(['/account/login']);
    //     //   },
    //     //   2000
    //     // );
    //
    //
    //   },
    //   error2 => {
    //     this.toastr.error(error2.error.error.message, '', {closeButton: true, timeOut: 2000});
    //
    //   }
    // );


  }
}
