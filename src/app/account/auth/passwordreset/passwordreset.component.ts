import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthenticationService } from '../../../core/services/auth.service';
import { environment } from '../../../../environments/environment';
import {ToastrService} from 'ngx-toastr';
import {LoginService} from '../../../services/authentification/login/login.service';
import {ResetPasswordService} from '../../../services/resetpassword/reset-password.service';
import {UserServiceService} from '../../../services/authentification/userService/user-service.service';
import {MustMatch} from '../../../Validator/must-match.validator';

@Component({
  selector: 'app-passwordreset',
  templateUrl: './passwordreset.component.html',
  styleUrls: ['./passwordreset.component.scss']
})

/**
 * Reset-password component
 */
export class PasswordresetComponent implements OnInit, AfterViewInit {

  resetForm: FormGroup;
  key: any;

  // tslint:disable-next-line: max-line-length
  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private resetPasswordService: ResetPasswordService,
              private loginService: LoginService ,
              private userService: UserServiceService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.key = this.route.snapshot.paramMap.get('key');


    this.resetForm = this.formBuilder.group({
      resetKey: [this.key],
      password: ['', [Validators.required ]],
      confirmPassword: ['', Validators.required] }, {
      validator: MustMatch('password', 'confirmPassword')
    });


  }

  ngAfterViewInit() {
  }

  // convenience getter for easy access to form fields
  get f() { return this.resetForm.controls; }

  /**
   * On submit form
   */
  onSubmit() {
    this.resetPasswordService.resetPassword(this.resetForm.getRawValue()).subscribe(
      (data) =>
      {
        if (localStorage.getItem('token'))
        {

              this.loginService.getUserInfo(localStorage.getItem('userID')).subscribe(
                (user) => {

                  user.firstConnexion = 0;
                  delete user.role;
                  this.userService.editUser(user, user.id).subscribe(
                    (data2) => {

                      this.toastr.success('Réinitialisation du mot de passe terminée avec succès',
                        '', { timeOut: 3000, closeButton: true});
                      this.router.navigate(['account/login']);
                    }
                  );
                }
              );


        }
        else
        {
          this.toastr.success('Réinitialisation du mot de passe terminée avec succès',
            '', { timeOut: 3000, closeButton: true});
          this.router.navigate(['account/login']);
        }

      },
      error2 => {
        this.toastr.error(error2.error.error.message,
          '', { timeOut: 3000, closeButton: true});
      }
    );
  }
}
