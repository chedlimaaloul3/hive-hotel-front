import {DeviseOption} from '../models/devise.model';

const deviseData: DeviseOption[] = [
  {
    id: 'USD',
    name: 'Dollar américain',
    flag: '../../../assets/images/devises/USD.svg',
    unite: 1,
    symbol: '$'
  },
  {
    id: 'EUR',
    name: 'Euro',
    flag: '../../../assets/images/devises/EUR.svg',
    unite: 1,
    symbol: '€'

  }, {
    id: 'GBP',
    name: 'Livre britannique',
    flag: '../../../assets/images/devises/GBP.svg',
    unite: 1,
    symbol: '£'

  },
  {
    id: 'TND',
    name: 'Dinar tunisien',
    flag: '../../../assets/images/devises/TND.svg',
    unite: 1,
    symbol: 'DT'

  },
  {
    id: 'AED',
    name: 'Dirham émirati',
    flag: '../../../assets/images/devises/AED.svg',
    unite: 10,
    symbol: 'د.إ'

  }, {
    id: 'CAD',
    name: 'Dollar canadian',
    flag: '../../../assets/images/devises/CAD.svg',
    unite: 1,
    symbol: 'CA$'

  }, {
    id: 'CHF',
    name: 'Franc suisse',
    flag: '../../../assets/images/devises/CHF.svg',
    unite: 10,
    symbol: 'Fr'

  }, {
    id: 'DKK',
    name: 'Couronne danoise',
    flag: '../../../assets/images/devises/DKK.svg',
    unite: 100,
    symbol: 'kr'

  }, {
    id: 'JPY',
    name: 'Yen',
    flag: '../../../assets/images/devises/JPY.svg',
    unite: 1000,
    symbol: '¥'

  }, {
    id: 'KWD',
    name: 'Dinar Koweïtien',
    flag: '../../../assets/images/devises/KWD.svg',
    unite: 1,
    symbol: 'KD'

  }, {
    id: 'CNY',
    name: 'Yuan renminbi',
    flag: '../../../assets/images/devises/CNY.svg',
    unite: 1,
    symbol: '元'

  }, {
    id: 'NOK',
    name: 'Couronne norvégienne',
    flag: '../../../assets/images/devises/NOK.svg',
    unite: 100,
    symbol: 'kr'

  }, {
    id: 'QAR',
    name: 'Riyal qatari',
    flag: '../../../assets/images/devises/QAR.svg',
    unite: 10,
    symbol: 'QR'

  }, {
    id: 'SAR',
    name: 'Riyal saoudien',
    flag: '../../../assets/images/devises/SAR.svg',
    unite: 10,
    symbol: 'SAR'

  }, {
    id: 'SEK',
    name: 'Couronne suédoise',
    flag: '../../../assets/images/devises/SEK.svg',
    unite: 10,
    symbol: 'kr'

  },
  {
    id: 'LYD',
    name: 'Dinar libyen',
    flag: '../../../assets/images/devises/LYD.svg',
    unite: 1,
    symbol: 'LD'

  },
  {
    id: 'BHD',
    name: 'Dinar bahreïn',
    flag: '../../../assets/images/devises/BHD.svg',
    unite: 1,
    symbol: 'BD'

  }

];
export {deviseData };
