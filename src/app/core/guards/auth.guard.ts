import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';


import {LoginService} from '../../services/authentification/login/login.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private loginServices: LoginService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    this.loginServices.CheckToken(localStorage.getItem('token')).subscribe(
      (data) =>
      {

      }
      ,
      error2 => {
        console.log(error2);
        localStorage.setItem('expired', 'true');

      }
    );

    if (!!localStorage.getItem('token') == true && localStorage.getItem('expired') == 'false') {
      return true;
    }
    else{
      this.router.navigate(['/account/login'], { queryParams: { returnUrl: state.url } });
      return false;

    }

    // not logged in so redirect to login page with the return url

  }
}
