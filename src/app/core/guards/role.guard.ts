import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {LoginService} from '../../services/authentification/login/login.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: LoginService
  ) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){

    this.authService.whoAmI().subscribe(
      (userId: any) =>
      {
        this.authService.getUserInfo(userId.id).subscribe(
          (userInfo) =>
          {
            if ( route.data.roles)
            {
              const filteredArray = userInfo.roles.filter(value => route.data.roles.includes(value));
              if (filteredArray.length === 0 ) {
                // role not authorised so redirect to home page
                this.router.navigate(['/']);
                return false;
              }
            }
            return true;
          }
        );
      }
    );
    return true;
  }

}
